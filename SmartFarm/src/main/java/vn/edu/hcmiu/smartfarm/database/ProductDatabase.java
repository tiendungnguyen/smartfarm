/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.edu.hcmiu.smartfarm.database;

import java.util.ArrayList;
import vn.edu.hcmiu.smartfarm.model.Product;

/**
 *
 * @author junnguyen
 */
public class ProductDatabase {
    
    private static ArrayList<Product> PRODUCT_DATABASE = new ArrayList<Product>();
    
    
    
    public static Product getProductByID(int id){
        
        Product product = null;
        
        
        for (Product productDB : PRODUCT_DATABASE) {
            if (productDB.getId()==id) {
                product = productDB;
                break;
            }
        }
        
        return product;
    }
    
    
    public static ArrayList<Product> getAllProducts(){
        return PRODUCT_DATABASE;
    }
    
    public static void addProduct(Product product){ 
        PRODUCT_DATABASE.add(product);
        
    }
    
    public static void updateProduct(int productId, Product product){
     
     
        for (Product productDB : PRODUCT_DATABASE) {
            if (productDB.getId()==productId) {
                productDB = product;
                break;
            }
        }        
        
    }
    
    
    public static void deleteProduct(int id){
        
        int deleteItemIndex = -1;
        
        for (Product productDB : PRODUCT_DATABASE) {
            if (productDB.getId()==id) {
                deleteItemIndex = PRODUCT_DATABASE.indexOf(productDB);
                break;
            }
        }
        
        if (deleteItemIndex!=-1) {
            PRODUCT_DATABASE.remove(deleteItemIndex);
        }
        
        
        
    }
}
