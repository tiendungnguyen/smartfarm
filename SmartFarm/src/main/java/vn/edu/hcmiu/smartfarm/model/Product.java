/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.edu.hcmiu.smartfarm.model;

import java.util.ArrayList;

/**
 *
 * @author junnguyen
 */
public class Product {
    
    private int id;
    private String name;
    private String desciption;
    private String imagePath;
    private FarmLocation farmLocation;
    private ProductSize productSize;
    private ArrayList<Fertilizer> listOfDertilizer;
    private ArrayList<ProductStatus> listOfProductStatus;

    public Product() {
    }

    public Product(int id, String name, String desciption, String imagePath, FarmLocation farmLocation, ProductSize productSize, ArrayList<Fertilizer> listOfDertilizer, ArrayList<ProductStatus> listOfProductStatus) {
        this.id = id;
        this.name = name;
        this.desciption = desciption;
        this.imagePath = imagePath;
        this.farmLocation = farmLocation;
        this.productSize = productSize;
        this.listOfDertilizer = listOfDertilizer;
        this.listOfProductStatus = listOfProductStatus;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesciption() {
        return desciption;
    }

    public void setDesciption(String desciption) {
        this.desciption = desciption;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public FarmLocation getFarmLocation() {
        return farmLocation;
    }

    public void setFarmLocation(FarmLocation farmLocation) {
        this.farmLocation = farmLocation;
    }

    public ProductSize getProductSize() {
        return productSize;
    }

    public void setProductSize(ProductSize productSize) {
        this.productSize = productSize;
    }

    public ArrayList<Fertilizer> getListOfDertilizer() {
        return listOfDertilizer;
    }

    public void setListOfDertilizer(ArrayList<Fertilizer> listOfDertilizer) {
        this.listOfDertilizer = listOfDertilizer;
    }

    public ArrayList<ProductStatus> getListOfProductStatus() {
        return listOfProductStatus;
    }

    public void setListOfProductStatus(ArrayList<ProductStatus> listOfProductStatus) {
        this.listOfProductStatus = listOfProductStatus;
    }

    
    
}
