/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.edu.hcmiu.smartfarm.model;

/**
 *
 * @author junnguyen
 */
public class ProductSize {
    private double h;
    private double w;
    private double d;

    public ProductSize() {
    }

    public ProductSize(double h, double w, double d) {
        this.h = h;
        this.w = w;
        this.d = d;
    }
    

    public double getH() {
        return h;
    }

    public void setH(double h) {
        this.h = h;
    }

    public double getW() {
        return w;
    }

    public void setW(double w) {
        this.w = w;
    }

    public double getD() {
        return d;
    }

    public void setD(double d) {
        this.d = d;
    }
    
    
    
    
}
