/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.edu.hcmiu.smartfarm.test;

import com.lib4i.common.utils.JSONUtils;
import com.lib4i.common.utils.RestfulWSClient;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBException;
import vn.edu.hcmiu.smartfarm.database.ProductDatabase;
import vn.edu.hcmiu.smartfarm.model.FarmLocation;
import vn.edu.hcmiu.smartfarm.model.Fertilizer;
import vn.edu.hcmiu.smartfarm.model.Product;
import vn.edu.hcmiu.smartfarm.model.ProductSize;
import vn.edu.hcmiu.smartfarm.model.ProductStatus;

/**
 *
 * @author junnguyen
 */
public class ProductWSTest {
    
    public void addProduct(int productId){
        
        Product product  = sampleProduct(productId);
        try {
            String jsonStr = JSONUtils.marshal(product, Product.class);
            RestfulWSClient restfulWSClient = new RestfulWSClient("54.190.46.209", "8080", "/SmartFarm/ws/product/add");
            restfulWSClient.callPutMethod(jsonStr);
        } catch (JAXBException ex) {
            Logger.getLogger(ProductWSTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        
    }
    
    
    public Product getProduct(int productId){
        
        try {
           
            RestfulWSClient restfulWSClient = new RestfulWSClient("54.190.46.209", "8080", "/SmartFarm/ws/product/read");
            String jsonStr = restfulWSClient.callPutMethod(String.valueOf(productId));
            //System.out.println(jsonStr);
            
            Product product = JSONUtils.unmarshal(jsonStr, Product.class);
            return product;
        } catch (Exception ex) {
            Logger.getLogger(ProductWSTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    public Product sampleProduct(int productId){
        
        FarmLocation farmLocation = new FarmLocation(10.799889, 106.663423);
        ProductSize productSize = new ProductSize(3.5, 5.0, 4.0);
        
        ArrayList<Fertilizer> listOfFertilizers = sampleFertilizers();
        ArrayList<ProductStatus> listOfProductStatus = sampleProductStatus();
        
        
        Product product = new Product(productId, "Dưa Hấu", "Dưa Hấu Long An", "http://pic.png", 
                    farmLocation, productSize, listOfFertilizers, listOfProductStatus);
        
          
       return product;
        
        
    }
    
    public ArrayList<Fertilizer> sampleFertilizers(){
        
        ArrayList<Fertilizer> listOfFertilizers = new ArrayList<Fertilizer>();
        
        {
        Fertilizer fertilizer = new Fertilizer(1, "N.P.K.Si - Đầu Mùa");
        listOfFertilizers.add(fertilizer);           
        }
        
        {
        Fertilizer fertilizer = new Fertilizer(2, "N.P.K.Si - Cuối Mùa");
        listOfFertilizers.add(fertilizer);           
        }
        
        
        return listOfFertilizers;
    }
    
    public ArrayList<ProductStatus> sampleProductStatus(){
        
        
        
        ArrayList<ProductStatus> listOfProductStatus = new ArrayList<ProductStatus>();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        
        {
            String dateInString = "21-05-2013";
            ProductSize productSize = new ProductSize(3.0, 4.5, 3.0);
            
            Date date = null;
            try {
                date = formatter.parse(dateInString);
            } catch (ParseException ex) {
                Logger.getLogger(ProductWSTest.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            
            ProductStatus productStatus = new ProductStatus(date, productSize);
            listOfProductStatus.add(productStatus);
        }
        
        {
            String dateInString = "22-05-2013";
            ProductSize productSize = new ProductSize(3.1, 4.6, 3.1);
            
            Date date = null;
            try {
                date = formatter.parse(dateInString);
            } catch (ParseException ex) {
                Logger.getLogger(ProductWSTest.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            
            ProductStatus productStatus = new ProductStatus(date, productSize);
            listOfProductStatus.add(productStatus);
        }
        
         {
            String dateInString = "23-05-2013";
            ProductSize productSize = new ProductSize(3.2, 4.7, 3.2);
            
            Date date = null;
            try {
                date = formatter.parse(dateInString);
            } catch (ParseException ex) {
                Logger.getLogger(ProductWSTest.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            
            ProductStatus productStatus = new ProductStatus(date, productSize);
            listOfProductStatus.add(productStatus);
        }
        
        
        
        return listOfProductStatus;
    }
    
}
