/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.edu.hcmiu.smartfarm.model;

/**
 *
 * @author junnguyen
 */
public class Fertilizer {
    private int id;
    private String fertilizerName;

    public Fertilizer() {
    }

    public Fertilizer(int id, String fertilizerName) {
        this.id = id;
        this.fertilizerName = fertilizerName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFertilizerName() {
        return fertilizerName;
    }

    public void setFertilizerName(String fertilizerName) {
        this.fertilizerName = fertilizerName;
    }

    

    
    
}
