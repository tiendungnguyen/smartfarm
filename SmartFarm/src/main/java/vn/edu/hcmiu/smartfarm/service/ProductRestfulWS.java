/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.edu.hcmiu.smartfarm.service;

import com.lib4i.common.utils.JSONUtils;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBException;
import vn.edu.hcmiu.smartfarm.database.ProductDatabase;
import vn.edu.hcmiu.smartfarm.model.Product;

/**
 *
 * @author junnguyen
 */

@Path("product")
public class ProductRestfulWS {
    @Context
    private UriInfo context;

    public ProductRestfulWS() {
    }
    
    
    
    @GET
    @Path("get")
    @Produces(MediaType.TEXT_PLAIN)
    public String getXml() {
        //TODO return proper representation object
        
        return "OK"; 
    }
    
    @PUT
    @Path("add")
    @Consumes("application/json")
    @Produces("application/json")
    public void addProduct(String jsonStr) {
        
        Product product = null;
        try {
            product = JSONUtils.unmarshal(jsonStr, Product.class);
        } catch (JAXBException ex) {
            Logger.getLogger(ProductRestfulWS.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        if (product!=null) {
            ProductDatabase.addProduct(product);
        }
        
        
        
    }
    
    
    
    
    @PUT
    @Path("read")
    @Consumes("application/json")
    @Produces("application/json")
    public String readProduct(String productId) {
        
       Product product = ProductDatabase.getProductByID(Integer.parseInt(productId));
       
       if (product!=null) {
           
           try {
               String jsonStr = JSONUtils.marshal(product, Product.class);
               return jsonStr;
           } catch (JAXBException ex) {
               Logger.getLogger(ProductRestfulWS.class.getName()).log(Level.SEVERE, null, ex);
           }
           
       }
       
        
        return "";
        
    }
    
    
}
