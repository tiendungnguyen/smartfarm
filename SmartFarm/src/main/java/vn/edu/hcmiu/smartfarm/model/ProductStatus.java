/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.edu.hcmiu.smartfarm.model;

import java.util.Date;

/**
 *
 * @author junnguyen
 */
public class ProductStatus {
    
    private Date date;
    private ProductSize productSize;

    public ProductStatus() {
    }

    public ProductStatus(Date date, ProductSize productSize) {
        this.date = date;
        this.productSize = productSize;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public ProductSize getProductSize() {
        return productSize;
    }

    public void setProductSize(ProductSize productSize) {
        this.productSize = productSize;
    }
    
    
    
}
